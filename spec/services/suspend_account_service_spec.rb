# frozen_string_literal: true

require 'rails_helper'

describe SuspendAccountService do
  describe '#call' do
    before do
      stub_jsonld_contexts!

      allow(ActivityPub::DeliveryJob).to receive(:perform_later)
      allow(Stories::Delete).to receive(:run!).and_return(true)
      allow(Comments::Delete).to receive(:run!).and_return(true)
    end

    subject do
      described_class.new.call(account)
    end

    let!(:account) { create(:account) }
    let!(:story) { create(:activitypub_post, account: account) }
    let!(:comment) { create(:activitypub_comment, account: account) }
    let!(:like) { create(:like, account: account) }
    let!(:notification) { create(:notification, recipient: account) }
    let!(:follow_request) { create(:follow_request, follower: account) }
    let!(:active_follow) { create(:follow, follower: account) }
    let!(:passive_follow) { create(:follow, following: account) }

    let!(:remote_alice) do
      create(:account, inbox_url: 'https://alice.com/inbox')
    end

    let!(:remote_bob) do
      create(:account, inbox_url: 'https://bob.com/inbox')
    end

    it 'soft deletes associated comments' do
      subject
      expect(Comments::Delete).to have_received(:run!).with(comment: comment)
    end

    it 'soft deletes associated stories' do
      subject
      expect(Stories::Delete).to have_received(:run!).with(story: story)
    end

    it 'soft deletes likes' do
      expect { subject }.to change(account.likes, :count).by(-1)
    end

    it 'soft deletes notifications' do
      expect { subject }
        .to change(account.notifications_as_recipient, :count)
        .by(-1)
    end

    it 'soft deletes follow requests' do
      expect { subject }.to change(account.follow_requests, :count).by(-1)
    end

    it 'soft deletes active follows' do
      expect { subject }.to change(account.active_follows, :count).by(-1)
    end

    it 'soft deletes passive follows' do
      expect { subject }.to change(account.passive_follows, :count).by(-1)
    end

    it 'sends a delete actor activity to all known inboxes' do
      original_queue_adapter = ActiveJob::Base.queue_adapter
      ActiveJob::Base.queue_adapter = :test

      subject

      expect(ActivityPub::DeliveryJob)
        .to have_received(:perform_later)
        .with(any_args, account.id, 'https://alice.com/inbox')

      expect(ActivityPub::DeliveryJob)
        .to have_received(:perform_later)
        .with(any_args, account.id, 'https://bob.com/inbox')

      ActiveJob::Base.queue_adapter = original_queue_adapter
    end
  end
end
