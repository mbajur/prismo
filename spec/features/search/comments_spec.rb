# frozen_string_literal: true

require 'rails_helper'

feature 'Searching comments' do
  let(:search_comments_page) { Search::CommentsPage.new }

  let!(:comment_a) { create(:activitypub_comment, content_source: 'Body of comment A') }

  before do
    comment_a.cache_body
  end

  scenario 'user searches comments' do
    search_comments_page.load
    expect(search_comments_page).to have_content 'Please write your search query in the input above'

    # Enters wrong query
    search_comments_page.search('wrongquery')
    expect(search_comments_page).to have_content 'No results have been found. Maybe try changing search criteria?'

    # Enters description query
    search_comments_page.search('Body of comment A')
    expect(search_comments_page).to have_comments(count: 1)
    expect(search_comments_page.comments.first).to have_content comment_a.content_source
  end
end
