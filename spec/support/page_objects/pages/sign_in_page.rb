# frozen_string_literal: true

class SignInPage < SitePrism::Page
  set_url '/auth/sign_in'
  set_url_matcher %r{\/auth\/sign_in}

  element :email_field, 'input#user_email'
  element :password_field, 'input#user_password'
  element :submit_button, '[type="submit"]'

  def sign_in_using(email, password)
    email_field.set email
    password_field.set password
    submit_button.click
  end
end
