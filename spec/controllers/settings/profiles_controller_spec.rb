require 'rails_helper'

describe Settings::ProfilesController, type: :controller do
  let!(:account) { create(:account) }
  let!(:user) { create(:user, account: account) }

  before do
    sign_in user
  end

  describe 'DELETE #destroy' do
    it 'schedules account suspension job' do
      expect(Accounts::SuspendJob).to receive(:perform_later).with(account.id, true)

      delete :destroy, params: { accounts_delete: { current_password: 'TestPass' } }
      expect(response).to redirect_to(:root)
    end
  end
end
