# frozen_string_literal: true

require 'rails_helper'

describe Accounts::CommentsController, type: :controller do
  render_views

  let(:account) { create(:account) }

  describe 'GET #hot' do
    subject { get :hot, params: { username: account.username } }

    it 'renders successfull response' do
      subject
      expect(response).to be_successful
    end
  end

  describe 'GET #recent' do
    subject { get :recent, params: { username: account.username } }

    it 'renders successfull response' do
      subject
      expect(response).to be_successful
    end
  end
end
