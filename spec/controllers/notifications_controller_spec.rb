# frozen_string_literal: true

require 'rails_helper'

describe NotificationsController, type: :controller do
  let!(:account) { create(:account) }
  let!(:user) { create(:user, account: account) }

  let!(:unread_notification) { create(:notification, recipient: account, seen: false) }
  let!(:read_notification) { create(:notification, recipient: account, seen: true) }

  before do
    sign_in user

    allow(Notifications::MarkAllAsRead).to receive(:run!).and_return(true)
  end

  shared_examples 'it marks all notifications as read' do
    it 'calls #mark_all_as_read' do
      expect(Notifications::MarkAllAsRead)
        .to receive(:run!).with(account: account)

      subject
    end
  end

  describe 'GET #index' do
    subject { get :index }

    it 'assigns @notifications' do
      subject

      expect(assigns(:notifications))
        .to match_array [unread_notification, read_notification]
    end

    it_behaves_like 'it marks all notifications as read'
  end

  describe 'GET #unread' do
    subject { get :unread }

    it 'assigns @notifications' do
      subject

      expect(assigns(:notifications))
        .to match_array [unread_notification]
    end

    it_behaves_like 'it marks all notifications as read'
  end
end
