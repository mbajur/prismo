# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :comment do
    body Faker::Lovecraft.paragraph
    body_html 'Cached body html'
    local true
    uuid { SecureRandom.uuid }

    account
    story

    trait :remote do
      local false
      uri 'https://mastodon.technology/users/username/statuses/123456'
      url 'https://mastodon.technology/users/username/statuses/123456'
      domain 'mastodon.technology'
    end

    trait :removed do
      removed { true }
    end
  end
end
