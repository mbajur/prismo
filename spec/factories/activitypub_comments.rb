# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :activitypub_comment do
    content_source Faker::Lovecraft.paragraph
    content 'Cached body html'
    uuid { SecureRandom.uuid }

    account
    association :parent, factory: :activitypub_post

    trait :remote do
      uri 'https://mastodon.technology/users/username/statuses/123456'
      url 'https://mastodon.technology/users/username/statuses/123456'
      domain 'mastodon.technology'
    end

    trait :removed do
      removed { true }
    end

    trait :local do
      uri { nil }
      url { nil }
    end

    after(:create) do |comment|
      comment.cache_root
    end
  end
end
