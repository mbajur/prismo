# frozen_string_literal: true

require 'rails_helper'

describe Comments::Delete do
  describe '#run' do
    before do
      stub_jsonld_contexts!

      allow(ActivityPub::DeliveryJob).to receive(:perform_later)
    end

    subject do
      described_class.run(comment: comment)
    end

    let!(:remote_alice) do
      create(:account, inbox_url: 'https://alice.com/inbox')
    end

    let!(:remote_bob) do
      create(:account, inbox_url: 'https://bob.com/inbox')
    end

    context 'when comment is local' do
      let!(:account) { create(:account) }
      let!(:comment) { create(:activitypub_comment, account: account) }

      it 'sends a delete activity to all known inboxes' do
        subject

        expect(ActivityPub::DeliveryJob)
          .to have_received(:perform_later)
          .with(any_args, account.id, 'https://alice.com/inbox')

        expect(ActivityPub::DeliveryJob)
          .to have_received(:perform_later)
          .with(any_args, account.id, 'https://bob.com/inbox')
      end
    end

    context 'when comment is remote' do
      let!(:comment) { create(:activitypub_comment, :remote) }

      it 'does not send delete activity to known inboxes' do
        subject

        expect(ActivityPub::DeliveryJob).to_not have_received(:perform_later)
      end
    end
  end
end
