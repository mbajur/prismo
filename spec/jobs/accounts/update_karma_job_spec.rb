# frozen_string_literal: true

require 'rails_helper'

describe Accounts::UpdateKarmaJob do
  let(:account) { create(:account) }

  describe '#perform' do
    subject do
      described_class.perform_now(account.id, resource_type, add_or_remove)
    end

    context 'when we want to increment posts karma' do
      let(:resource_type) { 'Story' }
      let(:add_or_remove) { 'add' }

      it 'increments account posts_karma' do
        expect { subject }.to change { account.reload.posts_karma }.by(1)
      end
    end

    context 'when we want to increment comments karma' do
      let(:resource_type) { 'Comment' }
      let(:add_or_remove) { 'add' }

      it 'increments account posts_karma' do
        expect { subject }.to change { account.reload.comments_karma }.by(1)
      end
    end

    context 'when we want to decrement posts karma' do
      let(:resource_type) { 'Story' }
      let(:add_or_remove) { 'remove' }

      it 'increments account posts_karma' do
        expect { subject }.to change { account.reload.posts_karma }.by(-1)
      end
    end

    context 'when we want to increment comments karma' do
      let(:resource_type) { 'Comment' }
      let(:add_or_remove) { 'remove' }

      it 'increments account posts_karma' do
        expect { subject }.to change { account.reload.comments_karma }.by(-1)
      end
    end
  end
end
