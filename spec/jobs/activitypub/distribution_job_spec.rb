# frozen_string_literal: true

require 'rails_helper'

describe ActivityPub::DistributionJob do
  subject { described_class.new }

  let(:remote_account) do
    create(:account, :remote, inbox_url: 'https://example.com')
  end

  let!(:story) { create(:activitypub_post) }

  let!(:remote_comment) do
    create(:activitypub_comment, :remote, account: remote_account, parent: story)
  end

  let!(:local_comment) { create(:activitypub_comment, parent: story) }

  before do
    stub_jsonld_contexts!
  end

  describe '#perform' do
    before do
      allow(ActivityPub::DeliveryJob).to receive(:perform_later)
    end

    it 'delivers to followers' do
      subject.perform('ActivityPubComment', local_comment.id)

      expect(ActivityPub::DeliveryJob)
        .to have_received(:perform_later)
        .with(any_args, local_comment.account_id, 'https://example.com')
    end
  end
end
