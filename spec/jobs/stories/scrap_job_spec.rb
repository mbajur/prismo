# frozen_string_literal: true

require 'rails_helper'

describe Stories::ScrapJob do
  describe '#perform_now' do
    subject { described_class.perform_now(story.id) }
    let(:link_body) { '' }

    before do
        stub_request(:get, 'https://example.com')
          .to_return(status: 200,
                     body: link_body,
                     headers: { 'Content-Type' => 'text/html' })
      end

    context 'when story has no url set' do
      let(:story) { create(:activitypub_post, :text) }

      it { is_expected.to eq nil }
    end

    context 'when story has url set' do
      let(:story) { create(:activitypub_post, :link, url: 'https://example.com') }

      it 'updates scrapped_at column' do
        expect { subject }.to(change { story.reload.settings.scrapped_at })
      end

      describe 'it saves page title' do
        let(:link_body) do
          '<title>Example page title</title>'
        end

        it do
          subject
          expect(story.reload.url_meta.title).to eq 'Example page title'
        end
      end

      describe 'it saves page description' do
        let(:link_body) do
          '<meta content="Example description" property="og:description">'
        end

        it do
          subject
          expect(story.reload.url_meta.description).to eq 'Example description'
        end
      end
    end

    context 'when story url is set to a pdf file' do
      let(:story) { create(:activitypub_post, :link, url: 'https://example.com/file.pdf') }

      before do
        stub_request(:get, 'https://example.com/file.pdf')
          .to_return(status: 200,
                    body: link_body,
                    headers: { 'Content-Type' => 'application/pdf' })
      end

      it 'does not save url meta' do
        expect { subject }.to_not change(UrlMeta.all, :count)
      end
    end
  end
end
