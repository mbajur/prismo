# frozen_string_literal: true

require 'rails_helper'

describe Notification, type: :model do
  it { is_expected.to belong_to(:author) }
  it { is_expected.to belong_to(:recipient) }
  it { is_expected.to belong_to(:notifable) }
  it { is_expected.to belong_to(:context) }
end
