# frozen_string_literal: true

require 'rails_helper'

describe ActivityPub::Activity::Create do
  let(:service) { described_class.new(json, sender) }
  let(:sender) { create(:account, :remote) }

  let(:json) do
    {
      '@context': 'https://www.w3.org/ns/activitystreams',
      id: [ActivityPub::TagManager.instance.uri_for(sender), '#foo'].join,
      type: 'Create',
      actor: ActivityPub::TagManager.instance.uri_for(sender),
      object: object_json
    }.with_indifferent_access
  end

  describe '#perform' do
    subject { service }

    context 'as a reply to story (root comment)' do
      let(:original_status) { create(:activitypub_post) }

      let(:object_json) do
        {
          id: [ActivityPub::TagManager.instance.uri_for(sender), '#bar'].join,
          type: 'Note',
          content: 'Lorem ipsum',
          inReplyTo: ActivityPub::TagManager.instance.uri_for(original_status)
        }
      end

      it 'creates comment' do
        expect { subject.perform }.to change(sender.comments, :count).by(1)
      end
    end

    context 'as a reply to comment' do
      let(:original_comment) { create(:activitypub_comment) }

      let(:object_json) do
        {
          id: [ActivityPub::TagManager.instance.uri_for(sender), '#bar'].join,
          type: 'Note',
          content: 'Lorem ipsum',
          inReplyTo: ActivityPub::TagManager.instance.uri_for(original_comment)
        }
      end

      it 'creates sub comment' do
        expect { subject.perform }
          .to change(original_comment.children, :count)
          .by(1)
      end
    end
  end

  describe '#parsed_content' do
    let(:json) { {} }
    subject { service.send(:parsed_content) }

    before do
      service.instance_variable_set(:@object, 'content' => content)
    end

    context 'when content is empty' do
      let(:content) { '' }

      it { is_expected.to eq '' }
    end

    context 'when content conains no html' do
      let(:content) { 'Raw content body' }

      it { is_expected.to eq 'Raw content body' }
    end

    context 'when content conains html' do
      let(:content) do
        'Raw <a class="link" href="https://mastodon.social/prismo">content</a> body'
      end

      it do
        is_expected.to eq 'Raw [content](https://mastodon.social/prismo) body'
      end
    end
  end
end
