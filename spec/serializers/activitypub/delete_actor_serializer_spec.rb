# frozen_string_literal: true

require 'rails_helper'

describe ActivityPub::DeleteActorSerializer do
  let(:account) { build(:account) }

  describe '#as_json' do
    it 'does not raise exception' do
      expect { described_class.new(account) }.to_not raise_exception
    end
  end
end
