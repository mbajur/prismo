module Gutentag
  module TagPatch
    def locked?
      false
    end

    def local?
      true
    end
  end
end

Rails.configuration.to_prepare do
  Gutentag::Tag.prepend(Gutentag::TagPatch)
end
