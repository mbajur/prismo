# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'
# Use sqlite3 as the database for Active Record
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
gem 'hiredis'
gem 'redis-namespace'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false
# gem 'active_model_serializers'
gem 'devise'
gem 'devise-i18n'
gem 'slim-rails'
gem 'gutentag', '~> 2.4'
gem 'search_object'
gem 'draper'
gem 'link_thumbnailer'
gem 'active_interaction', '~> 3.6'
gem 'shrine'
gem 'image_processing'
gem 'simple_form'
gem 'rails-settings-cached', github: 'huacnlee/rails-settings-cached', tag: 'v0.7.3'
gem 'pundit'
gem 'kaminari'
gem 'closure_tree'
gem 'oj'
gem 'validate_url'
gem 'active_link_to'
gem 'ox'
gem 'http'
gem 'goldfinger'
gem 'mario-redis-lock', '~> 1.2', require: 'redis_lock'
gem 'json-ld', '~> 2.2'
gem 'rdf-normalize', '~> 0.3'
gem 'sidekiq'
gem 'meta-tags'
gem 'granola'
gem 'reverse_markdown'
gem 'sentry-raven'
gem 'redcarpet'
gem 'pg_search'
gem 'onebox'
gem 'rinku'
gem 'webmention'
gem 'webmention-endpoint'
gem 'doorkeeper'

group :development, :test do
  gem 'dotenv-rails'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'bullet'
  gem 'rspec-rails', '~> 3.7'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'spring-commands-rspec'
  gem 'foreman'
  gem 'letter_opener'
  # gem 'slimkeyfy' # automatic slim translations. Conflicts with webmention
  gem 'better_errors'
  gem 'binding_of_caller'
end

group :test do
  gem 'factory_bot_rails', '~> 4.0'
  gem 'simplecov', require: false
  gem 'shoulda-matchers', '~> 3.1'
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'capybara-email'
  gem 'selenium-webdriver'
  gem 'site_prism', '~> 2.15'
  gem 'faker', require: false
  gem 'rails-controller-testing'
  gem 'webmock'
end

group :production do
  gem 'aws-sdk-s3', '~> 1.2'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
