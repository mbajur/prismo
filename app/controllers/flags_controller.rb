# frozen_string_literal: true

class FlagsController < ApplicationController
  layout 'application'

  before_action :set_flaggable_jumpbox_link
  before_action :set_liked_ids

  helper_method :flaggable
  helper_method :form_url

  def new
    redirect_to edit_resource_path if find_flag.present?
    authorize Flag

    @resource = flaggable
    @flag_form = Flags::Create.new
  end

  def edit
    redirect_to new_resource_path if find_flag.blank?
    authorize find_flag

    @flag_form = Flags::Update.new(flag: find_flag, summary: find_flag.summary)
  end

  def create
    authorize Flag

    outcome = Flags::Create.run(
      flag_params.merge(
        actor: current_account,
        flaggable: flaggable
      )
    )

    if outcome.valid?
      redirect_to flaggable_path, notice: 'Flag has been created'
    else
      @flag_form = outcome
      render :new
    end
  end

  def update
    flag = find_flag
    authorize flag

    outcome = Flags::Update.run(
      flag: flag,
      summary: params.fetch(:flag)[:summary],
      account: current_account
    )

    if outcome.valid?
      redirect_to flag.flaggable, notice: 'Flag has been updated'
    else
      @flag_form = outcome
      render :edit
    end
  end

  private

  def flaggable_model
    if params.key?(:story_id)
      ActivityPubPost
    else
      ActivityPubComment
    end
  end

  def flaggable_id_param
    case flaggable_model.to_s
    when 'ActivityPubPost' then :story_id
    when 'ActivityPubComment' then :comment_id
    end
  end

  def flaggable_path
    flaggable.is_a?(ActivityPubPost) ? story_path(flaggable) : comment_path(flaggable)
  end

  def flaggable
    @flaggable ||= flaggable_model.find(params[flaggable_id_param])
  end

  def flag_params
    params.require(:flag).permit(:summary)
  end

  def find_flag
    @find_flag ||= flaggable.flags.unresolved.find_by(actor: current_account)
  end

  def set_liked_ids
    case flaggable_model.to_s
    when 'ActivityPubPost' then set_account_liked_story_ids
    when 'ActivityPubComment' then set_account_liked_comment_ids
    end
  end

  def form_url
    case flaggable_model.to_s
    when 'ActivityPubPost' then story_flag_path(flaggable)
    when 'ActivityPubComment' then comment_flag_path(flaggable)
    end
  end

  def edit_resource_path
    case flaggable_model.to_s
    when 'ActivityPubPost' then edit_story_flag_path
    when 'ActivityPubComment' then edit_comment_flag_path
    end
  end

  def new_resource_path
    case flaggable_model.to_s
    when 'ActivityPubPost' then new_story_flag_path
    when 'ActivityPubComment' then new_comment_flag_path
    end
  end

  def set_flaggable_jumpbox_link
    case flaggable_model.to_s
    when 'ActivityPubPost' then set_jumpbox_link(Jumpbox::STORIES_LINK)
    when 'ActivityPubComment' then set_jumpbox_link(Jumpbox::COMMENTS_LINK)
    end
  end
end
