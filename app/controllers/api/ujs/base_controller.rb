# frozen_string_literal: true

class Api::Ujs::BaseController < ApplicationController
  rescue_from Pundit::NotAuthorizedError, with: :not_authorized_error
  rescue_from Prismo::Exceptions::UnauthenticatedError, with: :not_authenticated_error

  private

  def not_authorized_error
    head 403 and return
  end

  def not_authenticated_error
    head 401 and return
  end

  def user_needed
    unless current_user
      raise Prismo::Exceptions::UnauthenticatedError
    end
  end
end
