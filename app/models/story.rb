# frozen_string_literal: true

class Story < ApplicationRecord
  include HasUUIDConcern
  include ActivityPub::ObjectConcern
  include PgSearch

  Gutentag::ActiveRecord.call(self)

  HOT_DAYS_LIMIT = 40

  attr_accessor :tag_list

  pg_search_scope :search, against: { title: 'A', description: 'B' },
                           using: { tsearch: { prefix: true } }

  belongs_to :account, counter_cache: true, optional: true
  has_many :comments, dependent: :destroy
  belongs_to :url_meta, optional: true
  has_many :likes, as: :likeable
  belongs_to :group, optional: true
  has_many :flags, as: :flaggable

  validates :url, allow_blank: true, uniqueness: true

  delegate :thumb, :thumb_url, :thumb_data?,
           to: :url_meta,
           allow_nil: true

  def article?
    !url.present?
  end

  def link?
    url.present?
  end

  def object_type
    article? ? :article : :page
  end
end
