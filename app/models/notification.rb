# frozen_string_literal: true

class Notification < ApplicationRecord
  EVENT_KEYS = %w[comment_reply story_reply new_follower new_flag].freeze

  belongs_to :author, polymorphic: true
  belongs_to :recipient, polymorphic: true
  belongs_to :notifable, polymorphic: true, optional: true
  belongs_to :context, polymorphic: true, optional: true

  scope :seen, ->{ where(seen: true) }
  scope :not_seen, ->{ where(seen: false) }

  validates :event_key, inclusion: { in: EVENT_KEYS }
end
