# frozen_string_literal: true

module ActivityPub
  module ObjectConcern
    extend ActiveSupport::Concern

    included do
      scope :local, -> { where(uri: nil) }
      scope :remote, -> { where.not(uri: nil) }
    end

    class_methods do
      def find_local(uuid)
        args = { domain: nil }

        if uuid.is_a?(String) && uuid.include?('-')
          args[:uuid] = uuid
        else
          args[:id] = uuid
        end

        find_by(args)
      end
    end

    def local?
      uri.nil?
    end
  end
end
