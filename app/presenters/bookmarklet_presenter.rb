# frozen_string_literal: true

class BookmarkletPresenter < BasePresenter
  def initialize
  end

  def href
    URI.escape "javascript:(function(){javascript:location.href='#{h.new_story_url}?url='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title)})()"
  end
end
