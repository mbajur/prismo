# frozen_string_literal: true

class FollowButtonPresenter
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::TextHelper

  attr_reader :actor, :account

  def initialize(actor, account)
    @actor = actor
    @account = account
  end

  def following?
    return false if actor.blank?

    actor.following?(account)
  end

  def icon_class
    following? ? 'fe-user-check' : 'fe-user-plus'
  end

  def label
    following? ? 'Following' : 'Follow'
  end

  def root_class
    following? ? 'btn-secondary' : 'btn-primary'
  end
end
