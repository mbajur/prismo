# frozen_string_literal: true

class Comments::BroadcastChanges < ActiveInteraction::Base
  object :comment, class: ActivityPubComment

  def execute
    ActionCable.server.broadcast 'updates_channel', {
      event: 'comments.updated',
      data: ActivityPub::CommentSerializer.new(comment).data
    }
  end
end
