# frozen_string_literal: true

class ActivityPub::ProcessAccount < ActiveInteraction::Base
  include JsonLdHelper

  string :username
  string :domain
  interface :json

  # Should be called with confirmed valid JSON
  # and WebFinger-resolved username and domain
  def execute
    return if json['inbox'].blank? || unsupported_uri_scheme?(json['id'])

    @collections = {}

    RedisLock.acquire(lock_options) do |lock|
      if lock.acquired?
        @account        = ::Account.find_remote(@username, @domain)
        @old_public_key = @account&.public_key

        create_account if @account.nil?
        update_account
      else
        raise Mastodon::RaceConditionError
      end
    end

    return if @account.nil?

    after_key_change! if key_changed?

    @account
  rescue Oj::ParseError
    nil
  end

  private

  def uri
    json['id']
  end

  def create_account
    @account = Account.new
    @account.username    = username
    @account.domain      = domain
    @account.private_key = nil
  end

  def update_account
    @account.last_webfingered_at = Time.now.utc

    set_immediate_attributes!
    set_fetchable_attributes!

    # @account.save_with_optional_media!
    @account.save!
  end

  def set_immediate_attributes!
    @account.inbox_url               = json['inbox'] || ''
    @account.outbox_url              = json['outbox'] || ''
    @account.shared_inbox_url        = (json['endpoints'].is_a?(Hash) ? json['endpoints']['sharedInbox'] : json['sharedInbox']) || ''
    @account.followers_url           = json['followers'] || ''
    @account.url                     = url || @uri
    @account.uri                     = uri
    @account.display_name            = json['name'] || ''
    @account.bio                     = json['summary'] || ''
    @account.locked                  = json['manuallyApprovesFollowers'] || false
    @account.actor_type              = actor_type
  end

  def set_fetchable_attributes!
    @account.avatar_remote_url = image_url('icon') if image_url('icon').present?
    @account.public_key        = public_key || ''
    # @account.statuses_count    = outbox_total_items    if outbox_total_items.present?
    @account.following_count   = following_total_items if following_total_items.present?
    @account.followers_count   = followers_total_items if followers_total_items.present?
  end

  def after_key_change!
    RefollowWorker.perform_async(@account.id)
  end

  def actor_type
    if @json['type'].is_a?(Array)
      @json['type'].find { |type| ActivityPub::FetchRemoteAccount::SUPPORTED_TYPES.include?(type) }
    else
      @json['type']
    end
  end

  def image_url(key)
    value = first_of_value(@json[key])

    return if value.nil?
    return value['url'] if value.is_a?(Hash)

    image = fetch_resource_without_id_validation(value)
    image['url'] if image
  end

  def public_key
    value = first_of_value(@json['publicKey'])

    return if value.nil?
    return value['publicKeyPem'] if value.is_a?(Hash)

    key = fetch_resource_without_id_validation(value)
    key['publicKeyPem'] if key
  end

  def url
    return if @json['url'].blank?

    url_candidate = url_to_href(@json['url'], 'text/html')

    if unsupported_uri_scheme?(url_candidate) || mismatching_origin?(url_candidate)
      nil
    else
      url_candidate
    end
  end

  def mismatching_origin?(url)
    needle   = Addressable::URI.parse(url).host
    haystack = Addressable::URI.parse(uri).host

    !haystack.casecmp(needle).zero?
  end

  def outbox_total_items
    collection_total_items('outbox')
  end

  def following_total_items
    collection_total_items('following')
  end

  def followers_total_items
    collection_total_items('followers')
  end

  def collection_total_items(type)
    return if json[type].blank?
    return @collections[type] if @collections.key?(type)

    collection = fetch_resource_without_id_validation(json[type])

    @collections[type] = collection.is_a?(Hash) && collection['totalItems'].present? && collection['totalItems'].is_a?(Numeric) ? collection['totalItems'] : nil
  rescue HTTP::Error, OpenSSL::SSL::SSLError
    @collections[type] = nil
  end

  def key_changed?
    !@old_public_key.nil? && @old_public_key != @account.public_key
  end

  def lock_options
    { redis: Redis.current, key: "process_account:#{@uri}" }
  end
end
