# frozen_string_literal: true

module Stories
  class Update < CreateUpdateBase
    object :story, class: ActivityPubPost

    validates :url, url: { allow_blank: true }
    validate :url_or_description_required
    validate :title_update_time_limit

    def execute
      story.name = name if name?
      story.tag_names = tags if tag_list?
      story.content_source = content_source if content_source?
      story.url = url if url.present? && can_update_url?

      story.modified_at = Time.current
      story.modified_count += 1 if edit_grace_period_passed?

      if story.save
        after_story_save_hook(story)
      else
        errors.merge!(story.errors)
      end

      story
    end

    def persisted?
      true
    end

    private

    def after_story_save_hook(story)
      Stories::ScrapJob.perform_later(story.id) if story.url_changed?
      Stories::BroadcastChanges.run!(story: story)
      ActivityPub::UpdateDistributionJob.call_later(story) if story.local?
    end

    def title_update_time_limit
      return unless title_changed?

      limit = Setting.story_title_update_time_limit
      errors.add(:title, "can't be edited after #{limit} minutes") unless can_update_title?
    end

    def title_changed?
      name? && name != story.name
    end

    def can_update_title?
      ActivityPubPostPolicy.new(account.user, story).update_title?
    end

    def can_update_url?
      Stories::UpdatePolicy.new(account.user, story).update_url?
    end

    def edit_grace_period_passed?
      period = Setting.edit_counter_grace_period_minutes
      period.to_i.minutes.ago > story.created_at
    end
  end
end
