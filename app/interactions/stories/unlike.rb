# frozen_string_literal: true

class Stories::Unlike < ActiveInteraction::Base
  object :story, class: ActivityPubPost
  object :account

  def execute
    like = ::Like.find_by(account: account, likeable: story)

    if like.present?
      like.destroy
      Accounts::UpdateKarmaJob.perform_later(story.account.id, 'Story', 'remove')
    end

    like
  end
end
