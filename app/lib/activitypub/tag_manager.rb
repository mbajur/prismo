# frozen_string_literal: true
require 'singleton'

class ActivityPub::TagManager
  include Singleton
  include RoutingHelper

  CONTEXT = 'https://www.w3.org/ns/activitystreams'

  COLLECTIONS = {
    public: 'https://www.w3.org/ns/activitystreams#Public'
  }.freeze

  def url_for(target)
    return target.url if target.respond_to?(:local?) && !target.local?

    case target.object_type
    when :person
      account_url(target)
    when :page, :article
      story_url(target)
    when :comment
      comment_url(target)
    end
  end

  def uri_for(target)
    return target.uri if target.respond_to?(:local?) && !target.local?

    case target
    when Account then account_url(target)
    # when Story then story_url(target)
    when ActivityPubPost then story_url(target)
    # when Comment then comment_url(target)
    when ActivityPubComment then comment_url(target)
    end
  end

  def generate_uri_for(_target)
    URI.join(root_url, 'payloads', SecureRandom.uuid)
  end

  def activity_uri_for(target)
    raise ArgumentError, 'unsupported object type' unless %i(page note article activity).include?(target.object_type)
    raise ArgumentError, 'target must be a local activity' unless target.local?

    case target
    # when Story then story_url(target)
    when ActivityPubPost then story_url(target)
    # when Comment then comment_url(target)
    when ActivityPubComment then comment_url(target)
    when Account then account_url(target.username)
    end
  end

  def to(_resource)
    [COLLECTIONS[:public]]
  end

  def cc(_resource)
    [COLLECTIONS[:public]]
  end

  def local_uri?(uri)
    return false if uri.nil?

    uri  = Addressable::URI.parse(uri)
    host = uri.normalized_host
    host = "#{host}:#{uri.port}" if uri.port

    !host.nil? && (::TagManager.instance.local_domain?(host) || ::TagManager.instance.web_domain?(host))
  end

  def uri_to_local_id(uri, param = :id)
    path_params = Rails.application.routes.recognize_path(uri)
    path_params[param]
  end

  def smart_uri_to_resource(uri)
    return if uri.nil?

    path = Rails.application.routes.recognize_path(uri)

    case path[:controller]
    when 'stories' then uri_to_resource(uri, ActivityPubPost)
    when 'comments' then uri_to_resource(uri, Comment)
    end
  rescue ActionController::RoutingError
    uri_to_resource(uri, Comment) || uri_to_resource(uri, Story)
  end

  def uri_to_resource(uri, klass)
    return if uri.nil?
    return klass.find_by(uri: uri.split('#').first) unless local_uri?(uri)

    case uri
    when %r{\S+\/posts\/[a-zA-Z0-9-]+}
      ActivityPubPost.find_local(uri_to_local_id(uri))
    when %r{\S+\/comments\/[a-zA-Z0-9-]+}
      ActivityPubComment.find_local(uri_to_local_id(uri))
    when %r{\S+\/@\w+}
      Account.find_local(uri_to_local_id(uri, :username))
    end
  rescue ActiveRecord::RecordNotFound
    nil
  end
end
