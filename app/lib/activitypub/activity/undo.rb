# frozen_string_literal: true

class ActivityPub::Activity::Undo < ActivityPub::Activity
  def perform
    case object['type']
    when 'Like'
      undo_like
    when 'Follow'
      undo_follow
    end
  end

  private

  def undo_like
    resource = story_from_uri(target_uri) || comment_from_uri(target_uri)
    return if resource.nil? || !resource.account.local?

    case resource
    when ActivityPubPost
      Stories::Unlike.run!(story: resource, account: account)
    when ActivityPubComment
      Comments::Unlike.run!(comment: resource, account: account)
    end
  end

  def undo_follow
    target_account = account_from_uri(target_uri)

    return if target_account.nil? || !target_account.local?

    if @account.following?(target_account)
      @account.unfollow!(target_account)
    elsif @account.requested_follow?(target_account)
      FollowRequest.find_by(follower: @account, following: target_account)&.destroy
    end
  end

  def target_uri
    @target_uri ||= value_or_id(@object['object'])
  end
end
