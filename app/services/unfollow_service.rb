# frozen_string_literal: true

class UnfollowService
  def initialize(actor, account)
    @actor = actor
    @account = account
  end

  def call
    unfollow! || undo_follow_request!
  end

  private

  attr_reader :actor, :account

  def unfollow!
    follow = Follow.find_by(follower: actor, following: account)
    return unless follow

    follow.destroy!
    destroy_notifications
    follow
  end

  def undo_follow_request!
    follow_request = FollowRequest.find_by(follower: actor, following: account)
    return unless follow_request

    follow_request.destroy!
    follow_request
  end

  def destroy_notifications
    Notification.where(event_key: 'new_follower', author: actor, recipient: account)
                .destroy_all
  end
end
