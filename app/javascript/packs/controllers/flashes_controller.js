import Toastify from 'toastify-js'
import BaseController from './base_controller'

export default class extends BaseController {
  static targets = ['item']

  connect () {
    this.itemTargets.forEach((item) => {
      let dataset = item.dataset

      setTimeout(() => {
        Toastify({
          text: dataset.value,
          duration: 5000,
          close: true,
          gravity: 'bottom',
          className: this._toastifyClass(dataset.severity)
        }).showToast()
      }, 300)
    })

    this.eventBus.addEventListener('toasts:add', function ({ detail }) {
      this._handleToastCreated(detail)
    }.bind(this))
  }

  disconnect() {
    this.eventBus.removeEventListener('toasts:add', function ({ detail }) {
      this._handleToastCreated(detail)
    }.bind(this))
  }

  _toastifyClass (severity) {
    return {
      notice: 'toast toast-primary',
      success: 'toast toast-success',
      warning: 'toast toast-warning',
      error: 'toast toast-error',
    }[severity]
  }

  _handleToastCreated ({ text, severity }) {
    Toastify({
      text: text,
      duration: 5000,
      close: true,
      gravity: 'bottom',
      className: this._toastifyClass(severity)
    }).showToast()
  }
}
