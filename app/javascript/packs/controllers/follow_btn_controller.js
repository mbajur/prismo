import BaseController from "./base_controller"
import axios from 'axios'

export default class extends BaseController {
  toggle () {
    this.loading = true
    let req = axios.post(this.data.get('actionPath'))

    req.then((resp) => {
      this.element.outerHTML = resp.data
      this.loading = false

      // @todo enable it when time comes
      //
      // I'm commenting that out because stimuls acts weird in here.
      // See https://discourse.stimulusjs.org/t/this-element-outerhtml-not-catching-up-changes/526/2
      // this._showResultToast()
    })

    req.catch((error) => {
      this.loading = false
    })
  }

  _showResultToast () {
    if (this.following == 'true') {
      this.addToast({ text: `You're now following @${this.username}!`, severity: 'success' })
    } else {
      this.addToast({ text: `You're not following @${this.username} anymore`, severity: 'success' })
    }
  }

  _refreshLoadingClass () {
    if (this.loading) {
      this.element.classList.add('loading')
    } else {
      this.element.classList.remove('loading')
    }
  }

  get following () {
    return this.data.get('following')
  }

  get username () {
    return this.data.get('username')
  }

  get loading () {
    return this.data.get('loading') == 'true'
  }

  set loading (val) {
    this.data.set('loading', val)
    this._refreshLoadingClass()
  }
}
