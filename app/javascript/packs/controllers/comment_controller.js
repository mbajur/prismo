import axios from 'axios'
import BaseController from './base_controller'

export default class extends BaseController {
  static targets = [
    'likeBtn',
    'replyContainer',
    'editContainer',
    'bodyContainer',
    'actionsContainer',
    'topMeta'
  ]

  topMetaClicked (e) {
    // Make sure user did not clicked on any link in top meta target
    if (e.target == this.topMetaTarget) this.toogleClosed()
  }

  toggleLike () {
    let req = axios.post(this.likeBtnTarget.dataset.actionPath)

    req.then((resp) => {
      this._handleToggleLikeSuccess(resp)
    })
  }

  toogleClosed () {
    this.element.classList.toggle('comment--closed')
  }

  showReply () {
    let req = axios.get(this.replyContainerTarget.dataset.actionPath)

    req.then((resp) => {
      this.replyContainerTarget.innerHTML = resp.data
    })
  }

  showEdit () {
    let req = axios.get(this.editContainerTarget.dataset.actionPath)

    req.then((resp) => {
      this.element.classList.add('comment--editing')
      this.editContainerTarget.innerHTML = resp.data
    })
  }

  delete (e) {
    e.preventDefault()

    let result = confirm('Are you sure?')
    if (!result) return false

    let req = axios.delete(e.target.dataset.actionPath)

    req.then(() => {
      this.addToast({ text: 'Comment queued for removal', severity: 'success' })
    })
  }

  cancelEdit (e) {
    e.preventDefault()
    this.element.classList.remove('comment--editing')
  }

  loadNewComment (e) {
    e.preventDefault()

    let req = axios.get(e.target.getAttribute('href')),
        parser = new DOMParser(),
        id = e.target.dataset.commentId

    req.then((resp) => {
      let doc = parser.parseFromString(resp.data, "text/html"),
          incomingComment = doc.querySelector(`.comment[data-id="${id}"]`)

      e.target.parentNode.parentNode.outerHTML = incomingComment.outerHTML
    })
  }

  _handleToggleLikeSuccess (resp) {
    let parser = new DOMParser(),
        doc = parser.parseFromString(resp.data, "text/html"),
        incomingComment = doc.getElementsByClassName('comment')[0],

        currentInner = this.element.getElementsByClassName('comment__inner')[0],
        incommingInner = doc.getElementsByClassName('comment__inner')[0],

        currentClassList = this.element.classList,
        incomingClassList = incomingComment.classList

    // Take care of comment--liked class
    if (incomingClassList.contains('comment--liked')) {
      this.element.classList.add('comment--liked')
    } else {
      this.element.classList.remove('comment--liked')
    }

    // Replace comment body (everything except the child comments)
    currentInner.outerHTML = incommingInner.outerHTML
  }
}
