import BaseController from './base_controller'
import SmoothScroll from 'smooth-scroll'

const smoothScroll = new SmoothScroll()

export default class extends BaseController {
  static targets = ['rootComments', 'newReplyToastTpl']

  connect () {
    this.eventBus.addEventListener('comments:created', function (payload) {
      this.handleCommentsCreated(payload)
    }.bind(this))

    this.eventBus.addEventListener('comments:updated', function (payload) {
      this.handleCommentsUpdated(payload)
    }.bind(this))
  }

  disconnect () {
    this.eventBus.removeEventListener('comments:created', function (payload) {
      this.handleCommentsCreated(payload)
    }.bind(this))

    this.eventBus.removeEventListener('comments:updated', function (payload) {
      this.handleCommentsUpdated(payload)
    }.bind(this))
  }

  handleCommentsCreated({ detail }) {
    let newComment = detail.response[0].getElementsByClassName('comment')[0],
        newCommentId = newComment.getAttribute('data-id'),
        newCommentParentId = newComment.getAttribute('data-parent-id'),
        newCommentDepth = newComment.getAttribute('data-depth')

    // If depth is bigger than 1, choose parent comment by parent-id. If not,
    // choose root comments container
    let parentCommentContainerEl = parseInt(newCommentDepth) > 1 ?
      this._findComentChildrenElById(newCommentParentId) :
      this.rootCommentsTarget

    // Append comment at the end of comments tree
    parentCommentContainerEl.appendChild(newComment)

    // Scroll to new comment
    this._scrollToCommentById(newCommentId)
  }

  handleCommentsUpdated ({ detail }) {
    let comment = detail.response[0].getElementsByClassName('comment')[0],
        commentId = comment.getAttribute('data-id'),
        existingComment = this._findCommentElById(commentId)

    // Update comment node
    existingComment.outerHTML = comment.outerHTML

    // Scroll to comment
    this._scrollToCommentById(commentId)
  }

  _scrollToCommentById (id) {
    setTimeout(() => {
      let comment = this._findCommentElById(id)
      smoothScroll.animateScroll(comment, null, {
        offset: () => (window.innerHeight / 2)
      })
    }, 100)
  }

  _findCommentElById (id) {
    return this.element.querySelector(`[data-id="${id}"]`)
  }

  _findComentChildrenElById (id) {
    return this._findCommentElById(id).querySelector('.comment__children')
  }
}
