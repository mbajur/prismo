import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "deleteAccountModal", "passwordField" ]

  showDeleteModal() {
    this.deleteAccountModalTarget.classList.add("modal--is-visible")
    this.passwordFieldTarget.focus()
  }

  hideDeleteModal() {
    this.deleteAccountModalTarget.classList.remove("modal--is-visible")
  }
}
