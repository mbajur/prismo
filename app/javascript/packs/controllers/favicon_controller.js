import BaseController from "./base_controller"

const FAVICON_SIZE = 64

export default class extends BaseController {
  initialize () {
    this.originalHref = this.href
    this.hasUnread = false
  }

  _refreshIndicator () {
    if (this.hasUnread != true) {
      this.href = this.originalHref
      return
    }

    let canvas = document.createElement('canvas')
    canvas.width = FAVICON_SIZE
    canvas.height = FAVICON_SIZE

    let context = canvas.getContext('2d')
    let img = document.createElement('img')
    img.src = this.href

    img.onload = () => {
      context.drawImage(img, 0, 0, FAVICON_SIZE, FAVICON_SIZE)

      context.beginPath()
      context.arc(canvas.width - FAVICON_SIZE / 4, FAVICON_SIZE / 4, FAVICON_SIZE / 4, 0, 2 * Math.PI)
      context.fillStyle = '#FF0000'
      context.fill()

      this.href = canvas.toDataURL('image/png')
    }
  }

  set href (value) {
    this.element.href = value
  }

  get href () {
    return this.element.href
  }

  set hasUnread (value) {
    this.data.set('hasUnread', value)
    this._refreshIndicator()
  }

  get hasUnread () {
    return this.data.get('hasUnread') == 'true'
  }

  set originalHref(value) {
    this.data.set('originalHref', value)
  }

  get originalHref() {
    return this.data.get('originalHref')
  }
}
