import BaseController from "./base_controller"
import Popper from 'popper.js'

export default class extends BaseController {
  static targets = ['toggle', 'popover']

  connect() {
    this._popper = new Popper(this.toggleTarget, this.popoverTarget, {
      placement: this.placement,
      eventsEnabled: false
    })
  }

  disconnect() {
    this._popper.destroy()
  }

  hide() {
    this.opened = false
  }

  delayedOpen () {
    clearTimeout(this._timer)
    this._timer = setTimeout(() => {
      this.opened = true
    }, this.openDelay)
  }

  delayedClose (e) {
    clearTimeout(this._timer)
    this._timer = setTimeout(() => {
      this.opened = false
    }, this.closeDelay)
  }

  hide (e) {
    if (this.element.contains(e.target) == false) {
      this.opened = false
    }
  }

  set opened (value) {
    if (value) {
      this._popper.enableEventListeners()
      this._popper.update()
      this.popoverTarget.classList.add('popover-n--opened')
    } else {
      this.popoverTarget.classList.remove('popover-n--opened')
      this._popper.disableEventListeners()
    }
  }

  get placement() {
    return this.data.get('placement') || 'bottom'
  }

  get openDelay() {
    let data = this.data.get('open-delay')
    return data ? parseInt(data) : 300
  }

  get closeDelay() {
    let data = this.data.get('close-delay')
    return data ? parseInt(data) : 300
  }
}
