# frozen_string_literal: true

class ActivityPub::FollowSerializer < ActivityPub::BaseSerializer
  def data
    {
      id: id,
      type: 'Follow',
      actor: ActivityPub::TagManager.instance.uri_for(object.follower),
      object: ActivityPub::TagManager.instance.uri_for(object.following)
    }
  end

  def id
    ActivityPub::TagManager.instance.uri_for(object) ||
      [ActivityPub::TagManager.instance.uri_for(object.follower), '#follows/', object.id].join
  end
end
