# frozen_string_literal: true

class ActivityPub::OutboxSerializer < ActivityPub::BaseSerializer
  def data
    result = {
      id: object.id,
      type: type
    }

    result[:totalItems] = total_items if object.size.present?
    result[:next] = object.next if object.next.present?
    result[:prev] = object.prev if object.prev.present?
    result[:part_of] = object.part_of if object.part_of.present?

    result[:first] = object.first if object.first.present?
    result[:last] = object.last if object.last.present?

    result[:items] = items if (!object.items.nil? || page?) && !ordered?
    result[:ordered_items] = items if (!object.items.nil? || page?) && ordered?

    result
  end

  def type
    if page?
      ordered? ? 'OrderedCollectionPage' : 'CollectionPage'
    else
      ordered? ? 'OrderedCollection' : 'Collection'
    end
  end

  def total_items
    object.size
  end

  def items
    ActivityPub::StorySerializer.list(object.items).data
  end

  private

  def ordered?
    object.type == :ordered
  end

  def page?
    object.part_of.present?
  end
end
