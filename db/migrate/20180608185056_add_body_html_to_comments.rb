class AddBodyHtmlToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :body_html, :text
  end
end
