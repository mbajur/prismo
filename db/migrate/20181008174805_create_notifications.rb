class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.string :event_key, null: false
      t.references :author, polymorphic: true
      t.references :recipient, polymorphic: true
      t.references :notifable, polymorphic: true
      t.boolean :seen, default: false

      t.timestamps
    end
  end
end
