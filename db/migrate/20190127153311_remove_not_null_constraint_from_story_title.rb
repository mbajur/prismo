class RemoveNotNullConstraintFromStoryTitle < ActiveRecord::Migration[5.2]
  def up
    change_column :stories, :title, :string, null: true
  end
end
